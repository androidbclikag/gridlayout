package com.example.gridlayout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.twentieth_century.view.*
import kotlinx.android.synthetic.main.twenty_first_century.view.*

class RecyclerViewAdapter(private val myDataSet: ArrayList<ItemModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class TwentiethCentury(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val setParameters = myDataSet[adapterPosition]
            itemView.textView20.text = setParameters.title
            itemView.imageView20.setImageResource(setParameters.image)

        }

    }

    inner class TwentyFirstCentury(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val setParameters = myDataSet[adapterPosition]
            itemView.textView21.text = setParameters.title
            itemView.imageView21.setImageResource(setParameters.image)

        }
    }

    companion object {
        const val TWENTIETH_CENTURY = 20
        const val TWENTY_FIRST_CENTURY = 21
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TWENTIETH_CENTURY) {
            TwentiethCentury(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.twentieth_century, parent, false)
            )
        } else {
            TwentyFirstCentury(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.twenty_first_century, parent, false)
            )
        }
    }


    override fun getItemCount(): Int = myDataSet.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is TwentiethCentury)
            holder.onBind()
        else if (holder is TwentyFirstCentury)
            holder.onBind()

    }

    override fun getItemViewType(position: Int): Int {
        val itemModel = myDataSet[position]
        return if (itemModel.century == 20)
            TWENTIETH_CENTURY
        else
            TWENTY_FIRST_CENTURY

    }

}