package com.example.gridlayout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUp()

    }


    private fun setUp() {
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = RecyclerViewAdapter(items)
        setData()
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = true
            refresh()
            Handler().postDelayed({
                swipeRefresh.isRefreshing = false
                adapter.notifyDataSetChanged()
            }, 5000)


        }
    }


    private fun refresh() {
        items.clear()
        adapter.notifyDataSetChanged()
    }

    private fun setData() {
        val first = ItemModel(R.mipmap.aka_morchiladze, "აკა მორჩილაძე", 21)
        val second = ItemModel(R.mipmap.dato_turashvili, "დათო ტურაშვილი", 21)
        val third = ItemModel(R.mipmap.nodar_dumbadze, "ნოდარ დუმბაძე", 20)
        val fourth = ItemModel(R.mipmap.revaz_inanishvili, "რევაზ ინანიშვილი", 20)
        val fifth = ItemModel(R.mipmap.ilia_chavchavadze, "ილია ჭავჭავაზე", 20)
        val sixth = ItemModel(R.mipmap.galaktion_tabidze, "გალაკტიონ ტაბიძე", 20)
        val seventh = ItemModel(R.mipmap.giorgi_kekelidze, "გიორგი კეკელიძე", 21)
        val eight = ItemModel(R.mipmap.toresa_mossy, "ტორესა მოსი", 21)
        items.add(first)
        items.add(second)
        items.add(third)
        items.add(fourth)
        items.add(fifth)
        items.add(sixth)
        items.add(seventh)
        items.add(eight)


    }

}
